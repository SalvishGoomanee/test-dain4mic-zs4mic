#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include "depthflowprojection_cuda.cc"
#include "filterinterpolation_cuda.cc"
#include "flowprojection_cuda.cc"
#include "interpolation_cuda.cc"
#include "interpolationCh_cuda.cc"
#include "mindepthflowjrojection_cuda.cc"
#include "separableconv_cuda.cc"
#include "separableconvflow_cuda.cc"

// IMPORTANT ! The name given here MUST correspond to the module declared in CMake.
PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {

    load_depthflowprojection(m);
    load_filterinterpolation(m);
    load_flowprojection(m);
    load_interpolation(m);
    load_interpolationch(m);
    load_MinDepthFlowProjection(m);
    load_seperableconv(m);
    load_seperableconvflow(m);

}

