from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CUDAExtension
from compiler_args import nvcc_args, cxx_args

setup(
    name='pycuda',
    install_requires=['torch'],
    ext_modules=[
        CUDAExtension(
            name='pycuda',
            sources=['pycuda.cpp',
                     'depthflowprojection_cuda_kernel.cu',
                     'filterinterpolation_cuda_kernel.cu',
                     'flowprojection_cuda_kernel.cu',
                     'interpolation_cuda_kernel.cu',
                     'interpolationch_cuda_kernel.cu',
                     'mindepthflowprojection_cuda_kernel.cu',
                     'separableconv_cuda_kernel.cu',
                     'separableconvflow_cuda_kernel.cu'
                     ],
            extra_compile_args={'cxx': cxx_args, 'nvcc': nvcc_args}
        ),
    ],
    cmdclass={'build_ext': BuildExtension}
)
